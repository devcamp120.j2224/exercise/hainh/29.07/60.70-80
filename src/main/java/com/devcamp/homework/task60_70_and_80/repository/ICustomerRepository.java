package com.devcamp.homework.task60_70_and_80.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.homework.task60_70_and_80.model.CCustomer;

public interface ICustomerRepository extends JpaRepository<CCustomer, Long>{
    CCustomer findById(Integer id);
}
